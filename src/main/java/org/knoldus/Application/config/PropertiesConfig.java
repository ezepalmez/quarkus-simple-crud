package org.knoldus.Application.config;

import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 *
 * @author mi_cu
 */
@ConfigurationProperties("algo")
public class PropertiesConfig {

    public String text;
    public String otro;
    public String user;
}
