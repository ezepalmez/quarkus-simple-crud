package org.knoldus.Application.controller;

import org.knoldus.Application.entity.StudentEntity;
import org.knoldus.Application.model.Student;
import org.knoldus.Application.service.StudentService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import org.knoldus.Application.config.PropertiesConfig;
import org.knoldus.Application.model.Notes;
import org.knoldus.Application.model.User;

@Path("/api")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StudentController {

    @Inject
    StudentService studentService;
    @Inject
    PropertiesConfig properties;

    @GET
    public List<Student> get() {
        System.err.println("-->>> " + properties.text);
        return studentService.get();
    }

    @GET
    @Path("/notes")
    public List<Notes> getNotes() {
        System.err.println("-->>> " + properties.otro);
        return studentService.getNotes();
    }

    @GET
    @Path("/users")
    public List<User> getUsers() {
        System.err.println("-->>> " + properties.user);
        return studentService.getUsers();
    }

    @GET
    @Path("/admin/{iddecl}/{iddeta}")
    public String getAdmin(@PathParam("iddecl") Integer iddecl, @PathParam("iddeta") Integer iddeta) {
        return studentService.getAdmin(iddecl, iddeta);
    }

    @POST
    public StudentEntity create(Student student) {
        StudentEntity studentEntity = studentService.create(student);
        return studentEntity;
    }

    @PUT
    public StudentEntity update(Student student) {
        StudentEntity studentEntity = studentService.update(student);
        return studentEntity;
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") Long id) {
        studentService.delete(id);
    }
}
