package org.knoldus.Application.service;

import org.knoldus.Application.entity.StudentEntity;
import org.knoldus.Application.model.Student;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.knoldus.Application.entity.NotesEntity;
import org.knoldus.Application.entity.UserEntity;
import org.knoldus.Application.model.Notes;
import org.knoldus.Application.model.User;
import org.jboss.logging.Logger;

@ApplicationScoped
public class StudentService {

    private static final Logger LOG = Logger.getLogger(StudentService.class);
    @Inject
    EntityManager entitymanager;

    public List<Student> get() {
        LOG.info("Called log!!!");
        List<StudentEntity> listAll = StudentEntity.findAll().list();
        return listAll
                .stream()
                .map(ie -> {
                    return new Student(ie.id, ie.first_name, ie.last_name);
                })
                .collect(Collectors.toList());
    }

    @Transactional
    public StudentEntity create(Student student) {
        StudentEntity studentEntity = new StudentEntity();

        studentEntity.first_name = student.getFirst_name();
        studentEntity.last_name = student.getLast_name();
        studentEntity.persist();
        return studentEntity;
    }

    @Transactional
    public StudentEntity update(Student Student) {
        StudentEntity entity = StudentEntity.findById(Student.getId());

        entity.first_name = Student.getFirst_name();
        entity.last_name = Student.getLast_name();
        return entity;
    }

    @Transactional
    public void delete(Long id) {
        StudentEntity.deleteById(id);
    }

    public List<Notes> getNotes() {
        List<NotesEntity> listAll = NotesEntity.findAll().list();

        return listAll.stream().map(ie -> {
            return new Notes(ie.id, ie.text, ie.completed);
        }).collect(Collectors.toList());
    }

    public List<User> getUsers() {
        List<UserEntity> listAll = UserEntity.findAll().list();

        return listAll.stream().map(ie -> {
            return new User(ie.id, ie.name, ie.age, ie.salary);
        }).collect(Collectors.toList());
    }

    @Transactional
    public String getAdmin(Integer iddecl, Integer iddeta) {

        String storedProcedure = entitymanager.createNativeQuery("CALL sp_eliminar_det_decl(:v_id_decl, :v_id_deta, :c_id)")
                .setParameter("v_id_decl", iddecl)
                .setParameter("v_id_deta", iddeta)
                .setParameter("c_id", 0)
                .getSingleResult().toString();

        return storedProcedure;
    }
}
