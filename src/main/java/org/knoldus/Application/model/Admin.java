package org.knoldus.Application.model;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Admin {

    private String codigo;
    private String mensaje;
    private Integer idUsuario;
    private Date fecInicio;
    private Date fecFinal;
    private String indEstado;
}
