package org.knoldus.Application.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

    public Long id;
    public String name;
    public Integer age;
    public Long salary;

    public User(Long id, String name, Integer age, Long salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
}
