package org.knoldus.Application.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Notes {

    public Long id;
    public String text;
    public Boolean completed;

    public Notes(Long id, String text, Boolean completed) {
        this.id = id;
        this.text = text;
        this.completed = completed;
    }

}
