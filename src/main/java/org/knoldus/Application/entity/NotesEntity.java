package org.knoldus.Application.entity;

import javax.persistence.*;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import java.io.Serializable;

//@Entity
//@Table(name = "notes")
public class NotesEntity extends PanacheEntityBase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "text")
    public String text;

    @Column(name = "completed")
    public Boolean completed;
}
