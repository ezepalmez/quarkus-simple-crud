package org.knoldus.Application.entity;

import javax.persistence.*;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import java.io.Serializable;

@Entity
@Table(name = "users")
public class UserEntity extends PanacheEntityBase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "name")
    public String name;

    @Column(name = "age")
    public Integer age;
    
    @Column(name = "salary")
    public Long salary;
}
